# frozen_string_literal: true

require 'digest/sha1'
require 'dragonpay/configuration'
require 'dragonpay/response'
require 'dragonpay/transaction'
require 'dragonpay/version'

module Dragonpay
  class << self
    # Setups the global configuration object.
    # Example:
    #
    #   Dragonpay.dracarys do |config|
    #     config.merchant_id = 'merchant_id'
    #     config.secret_key  = 'secret_key'
    #     config.environment = 'production'
    #   end
    #
    def dracarys
      yield config if block_given?
    end

    # Takes a hash argument and returns a Dragonpay::Response object
    # Example:
    #
    # Dragonpay's callback/postback looks something like:
    # params = {
    #   'txnid':   '32VG5N06',
    #   'refno':   '8JK34YS2',
    #   'status':  'S',
    #   'message': '[000] A message from dragonpay',
    #   'digest':  'b7cb37...f0ebae'
    # }

    # res = Dragonpay.parse(params) # => #<Dragonpay::Response...>
    # res.valid? # => true (if digest checks out)
    #
    def parse(params)
      res = Response.new
      params
        .keep_if { |param| Response::PARAMS.include? param }
        .each    { |pair|  res.send("#{pair.first}=", pair.last) }
      res
    end

    # Returns the global configuration object
    def config
      @config ||= Dragonpay::Configuration.new
    end
  end
end
