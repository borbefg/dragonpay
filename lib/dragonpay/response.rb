# frozen_string_literal: true

module Dragonpay
  class Response
    PARAMS = %i[txnid refno status message digest].freeze

    attr_accessor(*PARAMS)

    def valid?
      digest == Digest::SHA1.hexdigest(to_s)
    end

    def invalid?
      not(valid?)
    end

    private

    def to_s
      PARAMS.first(4).map { |param| send(param) }
            .push(Dragonpay.config.secret_key)
            .join(':')
    end
  end
end
