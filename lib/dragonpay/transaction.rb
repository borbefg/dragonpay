# frozen_string_literal: true

module Dragonpay
  class Transaction
    PARAMS = %i[txnid amount ccy description email].freeze

    attr_accessor(*PARAMS)

    def digest
      Digest::SHA1.hexdigest(to_s)
    end

    def url
      uri = URI(Dragonpay.config.url)
      uri.query = URI.encode_www_form(to_h)
      uri.path  = '/Pay.aspx'
      uri.to_s
    end

    private

    def to_s
      PARAMS.map { |param| send(param) }
            .unshift(Dragonpay.config.merchant_id)
            .push(Dragonpay.config.secret_key)
            .join(':')
    end

    def to_h
      PARAMS.map { |param| [param, send(param)] }.to_h.update(
        merchantid: Dragonpay.config.merchant_id,
        digest: digest
      )
    end
  end
end
