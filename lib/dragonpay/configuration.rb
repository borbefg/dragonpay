# frozen_string_literal: true

module Dragonpay
  class Configuration
    HOST = 'https://test.dragonpay.ph'

    attr_writer :merchant_id, :secret_key, :environment

    define_method(:merchant_id) { ENV['DRAGONPAY_ID']     || @merchant_id }
    define_method(:secret_key)  { ENV['DRAGONPAY_SECRET'] || @secret_key  }
    define_method(:environment) { ENV['DRAGONPAY_ENV']    || ENV['RAILS_ENV'] || @environment || 'development' }
    define_method(:url) { environment == 'production' ? HOST.sub(/test/, 'gw') : HOST }
  end
end
