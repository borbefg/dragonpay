# frozen_string_literal: true

RSpec.describe Dragonpay do
  describe '.config' do
    it 'returns the same object every time' do
      expect(subject.config).to equal(subject.config)
    end
  end

  describe '.dracarys' do
    before(:example) do
      subject.dracarys do |dp|
        dp.merchant_id = 'Merchant'
        dp.secret_key  = 'Secret'
        dp.environment = 'NATURALHABITAT'
      end
    end

    # cleanup so other specs will not fail because of some randomly set config
    after(:example) { subject.instance_variable_set('@config', nil) }

    it 'configures the global configuration' do
      expect(subject.config.environment).to eq('NATURALHABITAT')
      expect(subject.config.merchant_id).to eq('Merchant')
      expect(subject.config.secret_key).to  eq('Secret')
    end
  end

  describe '.parse' do
    it 'returns a Dragonpay::Response object given a hash' do
      expect(subject.parse(Hash.new)).to be_a Dragonpay::Response
    end
  end
end
