# frozen_string_literal: true

module Dragonpay
  RSpec.describe Configuration do
    context 'with environment variables' do
      let(:default_config) do
        Hash[
          DRAGONPAY_ENV:    'an_environment',
          DRAGONPAY_ID:     'a_merchant_id',
          DRAGONPAY_SECRET: 'a_secret_key'
        ]
      end

      before(:example) { mock_env(default_config) }

      it 'has environment equal to DRAGONPAY_ENV' do
        expect(subject.environment).to eq('an_environment')
      end

      it 'has merchant_id equal to DRAGONPAY_ID' do
        expect(subject.merchant_id).to eq('a_merchant_id')
      end

      it 'has secret_key equal to DRAGONPAY_SECRET' do
        expect(subject.secret_key).to eq('a_secret_key')
      end
    end

    # see spec/support/config_reader.rb
    describe('#merchant_id')  { it_behaves_like 'a config reader', 'DRAGONPAY_ID'     }
    describe('#secret_key')   { it_behaves_like 'a config reader', 'DRAGONPAY_SECRET' }

    # see spec/support/config_writer.rb
    describe('#merchant_id=') { it_behaves_like 'a config writer' }
    describe('#secret_key=')  { it_behaves_like 'a config writer' }
    describe('#environment=') { it_behaves_like 'a config writer' }

    describe '#environment' do
      it_behaves_like 'a config reader', 'DRAGONPAY_ENV', 'development'

      context 'with no DRAGONPAY_ENV' do
        it 'returns RAILS_ENV' do
          mock_env(dragonpay_env: nil, rails_env: 'railsenv')
          expect(subject.environment).to eq('railsenv')
        end

        context 'with no RAILS_ENV' do
          it 'falls back to "development"' do
            expect(subject.environment).to eq('development')
          end
        end
      end
    end

    describe '#url' do
      it "defaults to dragonpay's test server" do
        expect(subject.url).to match(/test\.dragonpay\.ph/)
      end

      context 'in production' do
        it "points to dragonpay's live server" do
          mock_env(dragonpay_env: 'production')
          expect(subject.url).to match(/gw\.dragonpay\.ph/)
        end
      end
    end
  end
end
