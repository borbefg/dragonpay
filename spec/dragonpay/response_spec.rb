# frozen_string_literal: true

module Dragonpay
  RSpec.describe Response do
    it { is_expected.to be_invalid }

    %i[txnid refno status message digest].each do |method|
      it { is_expected.to respond_to(method) }
      it { is_expected.to respond_to("#{method}=") }
    end

    context 'with valid digest' do
      it 'returns true' do
        subject.digest = Digest::SHA1.hexdigest('::::')
        expect(subject).to be_valid
      end
    end

    describe '.to_s' do
      it 'includes the secret_key as last argument' do
        mock_env(dragonpay_secret: 'ultra_mega_super_secret_key')
        expect(subject.send(:to_s).split(':').last).to eq('ultra_mega_super_secret_key')
      end
    end
  end
end
