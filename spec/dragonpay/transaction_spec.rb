# frozen_string_literal: true

module Dragonpay
  RSpec.describe Transaction do
    Transaction::PARAMS.each do |method|
      it { is_expected.to respond_to(method) }
      it { is_expected.to respond_to("#{method}=") }
    end

    describe '.to_s' do
      it 'includes merchant_id as first argument' do
        mock_env(dragonpay_id: 'not-nil')
        expect(subject.send(:to_s).split(':').last).to eq('not-nil')
      end

      it 'includes secret_key as last argument' do
        mock_env(dragonpay_secret: 'also-not-nil')
        expect(subject.send(:to_s).split(':').last).to eq('also-not-nil')
      end
    end

    describe '#digest' do
      it 'returns a SHA1 digest' do
        digest = Digest::SHA1.hexdigest('::::::')
        expect(subject.digest).to eq(digest)
      end
    end

    describe '#url' do
      before(:example) do
        mock_env(dragonpay_id: 'not-nil', dragonpay_secret: 'also-not-nil')
        Transaction::PARAMS.each do |param|
          subject.send("#{param}=", param.to_s)
        end
      end

      it 'points to /Pay.aspx' do
        expect(subject.url).to match(%r{.*\/Pay\.aspx.*})
      end

      it 'does not include secret_key' do
        expect(subject.url).to_not match(/.*secret(_?)[kK]?ey.*/)
      end

      it 'includes merchant_id' do
        expect(subject.url).to match(/.*merchantid.*/)
      end

      it 'includes digest' do
        expect(subject.url).to match(/.*digest.*/)
      end

      Transaction::PARAMS.each do |param|
        it "includes #{param}" do
          expect(subject.url).to match(/^.*\&?#{param}.*$/)
        end
      end
    end
  end
end
