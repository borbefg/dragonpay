# frozen_string_literal: true

RSpec.shared_examples 'a config writer' do
  method = metadata[:parent_example_group][:description].delete('#')

  before(:example) { subject.send(method, 'my_config_value') }

  it "assigns '#{method.delete('=')}'" do
    expect(subject.send(method.delete('='))).to eq('my_config_value')
  end
end
