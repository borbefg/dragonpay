# frozen_string_literal: true

def mock_env(env_hash)
  stub_const('ENV', env_hash.transform_keys { |key| key.to_s.upcase })
end
