# frozen_string_literal: true

RSpec.shared_examples 'a config reader' do |env_name, default_value = nil|
  method = metadata[:parent_example_group][:description].delete('#')

  subject { described_class.new.send(method) }

  it "defaults to #{default_value || 'nil'}" do
    expect(subject).to eq(default_value)
  end

  context "when #{env_name} is set" do
    it "returns #{env_name}'s value" do
      mock_env(env_name => 'some-value')
      expect(subject).to eq('some-value')
    end
  end
end
