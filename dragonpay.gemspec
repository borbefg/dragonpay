# frozen_string_literal: true

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'dragonpay/version'

Gem::Specification.new do |spec|
  spec.files         = `git ls-files -z ':(exclude)spec'`.split("\x0")
  spec.name          = 'dragonpay'
  spec.summary       = 'A Ruby wrapper for Dragonpay API'
  spec.version       = Dragonpay::VERSION

  spec.authors       = ['Francis Borbe']
  spec.email         = ['borbefg@gmail.com']
  spec.homepage      = 'https://github.com/borbefg/dragonpay'
  spec.license       = 'MIT'

  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = 'lib'

  spec.post_install_message = 'A dragon has spawned!'
end
