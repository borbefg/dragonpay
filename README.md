## Installation

Run:
```bash
$ bundle add dragonpay
```

Or add this line to your application's Gemfile:
```ruby
gem 'dragonpay' # don't forget to 'bundle'
```

Or install it yourself:
```bash
$ gem install dragonpay
```

## Getting Started

### Configuration
This gem will load values from your system environment variables by default. Set these variables and you're good to go.
```bash
DRAGONPAY_ENV='production' # Falls back to RAILS_ENV if this is not set
DRAGONPAY_ID='your_merchant_id'
DRAGONPAY_SECRET='your_secret_key'
```
> NOTE:  These takes higher precedence than the values in the initializer. Make sure to unset these variables if you intend to use values in your initializer.

### Initializer
#### Rails 5.x
```ruby
# Run on your terminal
> rails credentials:edit

# Add your dragonpay credentials
development:
  dragonpay:
    merchant_id: your_test_merchant_id
    secret_key:  your_test_secret_key

production:
  dragonpay:
    merchant_id: your_live_merchant_id
    secret_key:  your_live_secret_key

# Update (or create one if you haven't already) config/initializers/dragonpay.rb
Dragonpay.dracarys do |config|
  config.merchant_id = Rails.application.credentials.dig(Rails.env.to_sym, :dragonpay, :merchant_id)
  config.secret_key  = Rails.application.credentials.dig(Rails.env.to_sym, :dragonpay, :secret_key)
end
```

#### Rails 4.x
```ruby
# config/secrets.yml
development:
  dragonpay:
    merchant_id: your_test_merchant_id
    secret_key:  your_test_secret_key

production:
  dragonpay:
    merchant_id: your_live_merchant_id
    secret_key:  your_live_secret_key

Dragonpay.dracarys do |config|
  config.merchant_id = Rails.application.secrets.dragonpay_id      # or ENV['DRAGONPAY_ID']
  config.secret_key  = Rails.application.secrets.dragonpay_secret  # or ENV['DRAGONPAY_SECRET']
end
```

## Creating Transactions
```ruby
transaction = Dragonpay::Transaction.new
transaction.txnid  = SecureRandom.hex(10).upcase
transaction.amount = "%.2f" % 1200
transaction.ccy    = 'PHP'
transaction.email  = 'person@example.com'
transaction.description = 'Example Transaction'
redirect_to transaction.url # => "https://test.dragonpay.ph/Pay.aspx?merchantid=..."
```

## Processing Callback/Postback
```ruby
class DragonpayController < ApplicationController
  before_action :parse_params

  def postback
    render plain: "result=OK" if @res.valid?
  end

  def callback
    if @res.invalid?
      return render :some_error_page
    end

    redirect_to :success
  end

  private

  def parse_params
    @res = Dragonpay.parse(params)
  end
end
```

## Development

After checking out the repo, run `bundle` to install dependencies. Then, run `rake spec` or `rspec` to run the tests. You can also run `bundle console` for an interactive prompt that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/borbefg/dragonpay.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
